﻿using System;

namespace Voyon.DotNet.Interview.Logic.Models
{
    public class TaskViewModel
    {
        public string Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsFinished { get; set; }

        public UserViewModel AssignedUser { get; set; }
        public string UserCreatedId { get; internal set; }
        public DateTime DateCreated { get; internal set; }
        public string UserLastUpdatedId { get; internal set; }
        public DateTime LastUpdated { get; internal set; }
    }
}