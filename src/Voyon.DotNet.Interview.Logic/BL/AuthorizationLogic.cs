﻿using Voyon.DotNet.Interview.Core.Models;
using Voyon.DotNet.Interview.Core.Repositories;
using Voyon.DotNet.Interview.Logic.Models;
using System;
using System.Linq;
using System.Web;

namespace Voyon.DotNet.Interview.Logic.BL
{
    public class AuthorizationLogic : IAuthorizationLogic
    {
        private const string COOKIE_NAME = "CurrentUser";

        private readonly IUserRepository _usersRepository;

        public AuthorizationLogic(IUserRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public LoginButtonViewModel GetLoginButton()
        {
            if(!TryGetCurrentUser(out var userObj))
            {
                return new LoginButtonViewModel
                {
                    ButtonLabel = "Login",
                    IsLoggedIn = false
                };
            }

            return new LoginButtonViewModel
            {
                ButtonLabel = "Logout",
                IsLoggedIn = true,
                CurrentUserName = userObj.Username
            };
        }

        public bool TryLogin(LoginViewModel loginViewModel)
        {
            if (loginViewModel?.Username == null || loginViewModel.Password == null)
                return false;

            var matchingUsers = _usersRepository.Get(u => u.Username.Equals(loginViewModel.Username) &&
                u.Password.Equals(loginViewModel.Password));

            if (matchingUsers == null || matchingUsers.Count() != 1)
                return false;

            CreateCookie(matchingUsers.FirstOrDefault()?.Id.ToString());
            return true;
        }

        public bool TryLogout()
        {
            RemoveCookie();
            return true;
        }

        private static void CreateCookie(string userid)
        {
            var currentUserCookie = new HttpCookie(COOKIE_NAME) {Value = userid, Expires = DateTime.Now.AddDays(1d)};

            HttpContext.Current.Response.Cookies.Remove(COOKIE_NAME);
            HttpContext.Current.Response.Cookies.Add(currentUserCookie);
        }

        private static void RemoveCookie()
        {
            if (HttpContext.Current.Request.Cookies[COOKIE_NAME] == null) return;
            var myCookie = new HttpCookie(COOKIE_NAME)
            {
                Expires = DateTime.Now.AddDays(-1d)
            };
            HttpContext.Current.Response.Cookies.Add(myCookie);
            HttpContext.Current.Session.Abandon();
        }

        public bool TryGetCurrentUser(out UserViewModel userObj)
        {
            userObj = null;

            var currentUserCookie = HttpContext.Current.Request.Cookies.Get(COOKIE_NAME);
            if (currentUserCookie == null)
                return false;

            if (!Guid.TryParse(currentUserCookie.Value, out var userGuid))
                return false;

            var user = _usersRepository.Get(userGuid);
            if (user == null)
                return false;

            userObj = new UserViewModel {Id = user.Id.ToString(), Username = user.Username};
            return true;
        }

    }
}
