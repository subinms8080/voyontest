﻿using Voyon.DotNet.Interview.Core.Models;
using Voyon.DotNet.Interview.Core.Repositories;
using Voyon.DotNet.Interview.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Voyon.DotNet.Interview.Logic.BL
{
    public class TaskLogic : ITaskLogic
    {
        private readonly ITaskRepository _tasksRepository;
        private readonly IUserRepository _usersRepository;
        private readonly IAuthorizationLogic _authLogic;

        public TaskLogic(ITaskRepository tasksRepository, IUserRepository usersRepository, IAuthorizationLogic authLogic)
        {
            _tasksRepository = tasksRepository;
            _usersRepository = usersRepository;
            _authLogic = authLogic;
        }

        public bool Add(TaskViewModel task)
        {
            var hasCurrentUse = _authLogic.TryGetCurrentUser(out var userObj);

            if (!hasCurrentUse)
                return false;

            return _tasksRepository.Create(new Task
            {
                Id = new Guid(task.Id),
                Title = task.Title,
                Description = task.Description,
                IsFinished = task.IsFinished,
                AssignedUserId = new Guid(task.AssignedUser.Id),
                DateCreated = DateTime.UtcNow,
                UserCreatedId = userObj.Id,
            });
        }

        public bool Delete(string id, TaskViewModel task)
        {
            return _tasksRepository.Delete(new Guid(id), new Task
            {
                Id = new Guid(task.Id)
            });
        }

        public bool Edit(string id, TaskViewModel task)
        {
            //Loading task from db to copy created user id and created date times. Actually its not a good way, best way is to load the object from json and update the only the changing properties and save it.
            var taskDb = Get(task.Id);

            var hasCurrentUse = _authLogic.TryGetCurrentUser(out var userObj);

            if (!hasCurrentUse)
                return false;

            return _tasksRepository.Update(new Guid(id), new Task
            {
                Id = new Guid(task.Id),
                Title = task.Title,
                Description = task.Description,
                IsFinished = task.IsFinished,
                AssignedUserId = new Guid(taskDb.AssignedUser.Id),
                UserCreatedId = taskDb.UserCreatedId,
                DateCreated = taskDb.DateCreated,
                UserLastUpdatedId = userObj.Id,
                LastUpdated = DateTime.UtcNow
            });
        }

        public TaskViewModel Get(string id)
        {
            var taskModel = _tasksRepository.Get(new Guid(id));

            return new TaskViewModel
            {
                Id = taskModel.Id.ToString(),
                Title = taskModel.Title,
                Description = taskModel.Description,
                IsFinished = taskModel.IsFinished,
                AssignedUser = new UserViewModel
                {
                    Id = taskModel.AssignedUserId.ToString(),
                    Username = _usersRepository.Get(taskModel.AssignedUserId).Username
                },
                UserCreatedId = taskModel.UserCreatedId,
                DateCreated = taskModel.DateCreated,
                UserLastUpdatedId =taskModel.UserLastUpdatedId,
                LastUpdated =taskModel.LastUpdated
            };
        }

        public IEnumerable<TaskViewModel> Get()
        {
            var tasks = _tasksRepository.Get();
            return tasks.Select(t => new TaskViewModel
            {
                Id = t.Id.ToString(),
                Title = t.Title,
                Description = t.Description,
                IsFinished = t.IsFinished,
                AssignedUser = new UserViewModel
                {
                    Id = t.AssignedUserId.ToString(),
                    Username = _usersRepository.Get(t.AssignedUserId).Username
                },
                UserCreatedId = t.UserCreatedId,
                DateCreated = t.DateCreated,
                UserLastUpdatedId = t.UserLastUpdatedId,
                LastUpdated = t.LastUpdated
            });
        }
    }
}
