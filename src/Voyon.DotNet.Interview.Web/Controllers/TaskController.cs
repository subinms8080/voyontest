﻿using Voyon.DotNet.Interview.Logic.BL;
using System.Web.Mvc;
using Voyon.DotNet.Interview.Logic.Models;

namespace Voyon.DotNet.Interview.Web.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskLogic _taskLogic;

        public TaskController(ITaskLogic taskLogic)
        {
            _taskLogic = taskLogic;
        }

        public ActionResult Index()
        {
            return View(_taskLogic.Get());
        }

        public ActionResult Edit(string id)
        {
            var task = _taskLogic.Get(id);
            if (task == null)
                return PartialView("Error");
            else
            {
                return View(task);
            }
        }
        [HttpPost]
        public ActionResult Edit(TaskViewModel task)
        {
            var isUpdated= _taskLogic.Edit(task.Id, task);
            if (isUpdated)
                return RedirectToAction(nameof(Index));
            else
            {
                return View(task);
            }
        }

       
    }
}